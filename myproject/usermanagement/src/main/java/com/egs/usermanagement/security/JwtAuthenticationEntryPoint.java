package com.egs.usermanagement.security;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.egs.common.logging.LogService;
import com.egs.common.logging.LogServiceFactory;
import com.egs.common.vo.ApiResponseVO;
import com.fasterxml.jackson.databind.ObjectMapper;

/***
 * Throw exception due to an unauthenticated user trying to access a
 * resource*that requires authentication.**
 * 
 * @author Son Nguyen
 *
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private static final LogService logger = LogServiceFactory.create(JwtAuthenticationEntryPoint.class);

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
			throws IOException, ServletException {

		final String MN = "commence";
		logger.errorMessage(MN, "Authentication Error: \n{0}", e);

		ApiResponseVO<Object> reponseVO = new ApiResponseVO<>(false, "UnAuthoried", null);
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		OutputStream out = response.getOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(out, reponseVO);
		out.flush();
	}

}
