package com.egs.usermanagement;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RequestCallback;

public class OAuth2AuthTokenCallback implements RequestCallback {

	private static final FormHttpMessageConverter FORM_MESSAGE_CONVERTER = new FormHttpMessageConverter();

	private MultiValueMap<String, String> form;

	private Map<String, String> requestHeaders;

	public OAuth2AuthTokenCallback(MultiValueMap<String, String> form, Map<String, String> requestHeaders) {
		this.form = form;
		this.requestHeaders = requestHeaders;
	}

	public void doWithRequest(ClientHttpRequest request) throws IOException {
		final HttpHeaders clientHeaders = request.getHeaders();
		for (final Map.Entry<String, String> entry : requestHeaders.entrySet()) {
			clientHeaders.add(entry.getKey(), entry.getValue());
		}
		request.getHeaders()
				.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED));
		FORM_MESSAGE_CONVERTER.write(this.form, MediaType.APPLICATION_FORM_URLENCODED, request);
	}
}
