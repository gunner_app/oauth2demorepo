package com.egs.usermanagement.stepdefs;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.egs.usermanagement.CucumberSpringContextConfiguration;
import com.egs.usermanagement.HttpRequestExecutor;
import com.egs.usermanagement.ResponseResult;
import com.egs.usermanagement.vo.UserVO;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UserControllerTest extends CucumberSpringContextConfiguration {

	@Autowired
	private HttpRequestExecutor httpRequestExecutor;

	private String accessToken;

	@Given("^Login successfully$")
	public void Login_successfully() {
		ResponseResult result = httpRequestExecutor.executeLogin();
		try {
			JSONObject jsonBody = new JSONObject(result.getBody());
			accessToken = jsonBody.getString("access_token");
			assertNotNull(accessToken);
			assertEquals(HttpStatus.OK, result.getTheResponse().getStatusCode());
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
	}

	@When("^Creating new user$")
	public void Creating_new_user() {
		UserVO vo = new UserVO();
		vo.setUsername("admin12");
		vo.setEmail("admin12@gmail.com");
		vo.setFirstName("Admin");
		vo.setLastName("Master");
		vo.setPassword("123456");

		ObjectMapper ojm = new ObjectMapper();
		String body;
		try {
			body = ojm.writeValueAsString(vo);
			ResponseResult result = httpRequestExecutor.executePost("/users", body, accessToken);

			assertEquals(HttpStatus.OK, result.getTheResponse().getStatusCode());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Then("^The user can be retrieved from database$")
	public void The_user_can_be_retrieved_from_database() {
		try {
			ResponseResult result = httpRequestExecutor.executeGet("/users", accessToken);
			assertEquals(HttpStatus.OK, result.getTheResponse().getStatusCode());

			ObjectMapper ojm = new ObjectMapper();
			UserVO[] users = ojm.readValue(result.getBody(), UserVO[].class);
			UserVO expectedUser = null;
			for (UserVO userVO : users) {
				if ("admin12".equals(userVO.getUsername())) {
					expectedUser = userVO;
					break;
				}
			}

			assertNotNull(expectedUser);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
