package com.egs.usermanagement.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_code")
public class OauthCode {

	@Id
	private String code;

	private byte authentication;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public byte getAuthentication() {
		return authentication;
	}

	public void setAuthentication(byte authentication) {
		this.authentication = authentication;
	}

}
