package com.egs.usermanagement.vo;

import java.util.List;

import com.egs.common.vo.AuditingVO;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserVO extends AuditingVO {
	private String username;

	private String email;

	@JsonProperty(value = "password", access = JsonProperty.Access.WRITE_ONLY)
	private String password;

	private String firstName;

	private String lastName;

	private List<String> roles;

	public UserVO() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
